import java.util.*;

class Main {
  public static void main(String[] args) {

        System.out.println("Enter user id");

        HashMap<Integer, String> passportsAndNames = new HashMap<>();

        passportsAndNames.put(212133, "Lidia Arkadievna Bublikova");
        passportsAndNames.put(162348, "Ivan Mikhailovich Serebryakov");
        passportsAndNames.put(808277,"Donald John Trump");

        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();

        String Name = passportsAndNames.get(id);

        if(Name != null){
          System.out.println(Name);
        }
        else{
          System.out.println("User not found!");
        }   
  }
}
